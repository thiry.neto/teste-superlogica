var ajax;
ajax = ajax || (function () {

    return {
        post_form : function(_form)
        {
            let action = _form.attr('action');

            return $.ajax({
                url: action,
                method: "POST",
                data: _form.serialize(),
                dataType: "json"
            });
        },
        get : function(_url)
        {
            return $.ajax({
                url: _url,
                method: "GET",
                dataType: "json"
            });
        },
    }

})();

$(document).ready(function() {

    $(document).on('click', '#btn-preecnher', function (e) {
        
        let get = ajax.get($(this).data('url'));
        
        get.done(function(data) {

            console.log(data);
            
            if (data.html) {

                $.each(data.html, function(seletor, value) {

                    if ($(seletor)[0]) {
                        
                        (typeof ($(seletor).attr('value')) != 'undefined') ? $(seletor).val(value) : $(seletor).html(value);
                    }
                })
            }
        });

        get.fail(function(data){
            alert('Erro!');
        });

    });
    
    $(document).on('click', '.btn-submit', function (e) {
        let el_form = $(this).closest('form');
        let post = ajax.post_form(el_form);

        post.done(function(data) {
            
            if (data.html) {

                $.each(data.html, function(seletor, value) {

                    if ($(seletor)[0]) {
                        
                        (typeof ($(seletor).attr('value')) != 'undefined') ? $(seletor).val(value) : $(seletor).html(value);
                    }
                })
            }
        });

        post.fail(function(data){
            alert('Erro!');
        });
    });

});

