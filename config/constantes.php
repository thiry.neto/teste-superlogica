<?php
define('REGEX_SENHA_8_DIGITOS_LETRA_NUMERO', '^(?=.*[a-zA-z])(?=.*\d)([a-zA-Z\d~!@\#$%^&*()\[\]+\-\.;\{\}\/\\=,:]){8,}$');
define('REGEX_CEP', '^(\d{5}-\d{3})$');
define('REGEX_TELEFONE', '^\(?\d{2}\)?[\s-]?\d{5}-?\d{4}$');
define('REGEX_EMAIL', '^([0-9a-zA-Z]+([_.-]?[0-9a-zA-Z]+)*@[0-9a-zA-Z]+[0-9,a-z,A-Z,.,-]*(.){1}[a-zA-Z]{2,4})+$');