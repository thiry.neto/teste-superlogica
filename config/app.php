<?php
require 'constantes.php';
require __DIR__ . '/../classes/Validate.php';
require __DIR__ . '/../classes/Data.php';
require __DIR__ . '/../classes/Alert.php';
require __DIR__ . '/../classes/Repository.php';
require __DIR__ . '/../classes/Common.php';
require __DIR__ . '/../classes/Curl.php';