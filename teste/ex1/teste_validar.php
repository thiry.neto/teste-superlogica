<?php
require '../../config/app.php';

try {

    $preencher = (isset($_GET['preencher'])) ? true : false;
    $id = '';

    if ($preencher === false) {

        $regras = [
            'userName' => ['rules' => [[Validate::TYPE_REQUIRED], [Validate::TYPE_SIZE, [3,60]]], 'label' => 'Nome do usuário'],
            'zipCode' => ['rules' => [Validate::TYPE_REQUIRED, [Validate::TYPE_CEP]], 'label' => 'CEP'],
            'phoneNumber' => ['rules' => [Validate::TYPE_REQUIRED, [Validate::TYPE_FONE]], 'label' => 'Telefone'],
            'email' => ['rules' => [Validate::TYPE_REQUIRED, [Validate::TYPE_EMAIL]], 'label' => 'Email'],
            'password' => ['rules' => [Validate::TYPE_REQUIRED, [Validate::TYPE_REGEX, REGEX_SENHA_8_DIGITOS_LETRA_NUMERO, 'Informar uma senha que tenha no mínimo 8 dígitos, sendo 1 letra e 1 número!']], 'label' => 'Senha'],
        ];
        
        // validar entradas
        $validar = Validate::validar($regras, $_POST);
    
        if ($validar !== true) {
    
            throw new Exception(Alert::warning(implode('<br>', $validar)));
        }
        
        // filtrar variaveis $_POST para retornar apenas os indices esperados na regra de validacao
        $informacao = filtrarArrayChave($_POST, array_keys($regras));
        
        // verificar se email já esta sendo utilizado, fazer consulta
        $data = new Data();
        $buscar_email_utilizado = $data->select('usuario', $informacao);
    
        if ($buscar_email_utilizado === true) {
    
            throw new Exception(Alert::warning('O e-mail informado já está sendo utilizado!'));
        }
        
        // salvar registro
        $id = $data->insert('usuario', $informacao);
        
        if ($id === false) {
            
            throw new Exception(Alert::warning('Erro ao salvar registro!'));
        }
        
        $response_html = ['#userID' => $id, '#content-resultado-validar' => Alert::success('Ok, dados válidos!')];
    }
    else {

        $response_html = ['#userID' => '', '#content-resultado-validar' => Alert::success('Ok, dados preenchidos!')];

        foreach ($_POST as $indice => $value) {

            $response_html[sprintf('#%s', $indice)] = $value;
        }
    }
    
    echo json_encode(['success' => true, 'html' => $response_html]);
} catch (Exception $ex) {
    
    echo json_encode(['error' => true, 'html' => ['#content-resultado-validar' => $ex->getMessage()]]);
}
