<?php
require '../../config/app.php';

class Arr
{
    private $array = [];

    public function __construct(array $array = [])
    {
        $this->array = $array;
    }
    
    public function popular(int $lengh = 7)
    {
        $this->array = array_map(function($value) {
            return gerarNumeroAleatorio(1, 10000) * $value;
        }, array_fill(0, $lengh, 1));
    }

    public function print_position(int $position = 3)
    {
        return $this->array[$position - 1];
    }

    public function implode_array_values(string $glue = ',')
    {
        return implode($glue, array_values($this->array));
    }

    public function value_exists(int $value = 14)
    {
        return (in_array($value, $this->array)) ? true : false;
    }

    public function compare_value()
    {
        $value = 0;
        return $this->array = array_filter($this->array, function($v) use (&$value) {
            
            $return = ($v > $value) ? true : false;
            $value = $v;
            if ($return === true) {

                return $v;
            }
        });
    }

    public function remove_last_position()
    {
        return $this->array = array_slice($this->array, 0, -1);
    }

    public function count()
    {
        return count($this->array);
    }

    public function reverse()
    {
        return $this->array = array_reverse($this->array);
    }
    
    public function show()
    {
        return $this->array;
    }
}

// criando array
$array = new Arr();

// popular o array
$array->popular();
echo 'Populando o array...<br>';
var_dump($array->show());
echo '<br><br>';

echo 'Mostrando a posição 3 do array: ' . $array->print_position() . '<br><br>';

$array_string = $array->implode_array_values();
echo 'Criando variável com as posições do array: ' . $array_string . '<br><br>';

// novo array
echo 'Criando novo array a partir da string acima...<br>';
$new_array = new Arr(explode(',', $array_string));
var_dump($new_array->show());
echo '<br><br>';

echo 'Verificar se o valor 14 existe no array...';
echo ($new_array->value_exists(14)) ? 'Valor existe!!!' : 'Valor não existe!!!';
echo '<br><br>';

echo 'Eliminar número menors que o anterior...<br><br>';
var_dump($new_array->compare_value());
echo '<br><br>';

echo 'Eliminar última posição do array...<br><br>';
var_dump($new_array->remove_last_position());
echo '<br><br>';

echo 'Quantidade do array...<br><br>';
var_dump($new_array->count());
echo '<br><br>';