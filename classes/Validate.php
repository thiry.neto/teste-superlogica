<?php

class Validate
{
    private static $erros;

    const TYPE_REQUIRED = 'required';
    const TYPE_FONE = 'telefone';
    const TYPE_CEP = 'cep';
    const TYPE_SIZE = 'size';
    const TYPE_REGEX = 'regex';
    const TYPE_EMAIL = 'email';
    
    private function __construct()
    {
    }

    public static function validar($parametros, $dados)
    {
        self::$erros = [];
        
        foreach ($parametros as $nome_campo_dados => $regras)
        {
            if (!isset($dados[$nome_campo_dados])) 
            {
                throw new Exception(sprintf('O campo %s não foi informado', $parametros[$nome_campo_dados]['label']));
            }

            foreach ($regras['rules'] as $regra)
            {
                if (!is_array($regra)) {

                    $regra = [$regra];
                }
                
                // nome do metodo da regra
                $method = "validar_" . $regra[0];
                
                // verificar se o metodo existe
                if (!method_exists(self::class, $method)) 
                {
                    throw new Exception('Não foi possível encontrar o método ' . $method . ' de validação!');
                }
                
                $valor_regra = (isset($regra[1])) ? $regra[1] : null;
                $mensagem_erro_regra = (isset($regra[2])) ? $regra[2] : '';
                call_user_func_array(array(self::class, $method), [$dados[$nome_campo_dados], $valor_regra, $regras['label'], $mensagem_erro_regra]);
            }
        }
        
        if (!empty(self::$erros)) 
        {
            return self::error();
        }
        
        return true;
    }

    protected static function validar_required($valor_para_validar, $valor_regra = true, $nome_campo = '', $mensagem = '')
    {
        $validar = (($valor_regra === true) && ((empty($valor_para_validar)) && (!is_numeric($valor_para_validar)))) ? false : true;

        if ($validar !== true) 
        {
            self::gravar_erro(sprintf('O valor do campo %s deve ser preenchido', $nome_campo), $mensagem);
        }

        return $validar;
    }
    
    protected static function validar_email($valor_para_validar, $valor_regra = null, $nome_campo = '', $mensagem = '')
    {
        $validar = (filter_var($valor_para_validar, FILTER_VALIDATE_EMAIL)) ? true : false;
        
        if (!$validar) {
            
            self::gravar_erro(sprintf('O valor do campo %s deve ter um valor válido', $nome_campo), $mensagem);
        }
        
        return $validar;
    }

    protected static function validar_regex($valor_para_validar, $valor_regra, $nome_campo = '', $mensagem = '')
    {
        $validar = (!empty($valor_regra)) ? preg_match('#' . $valor_regra . '#', $valor_para_validar, $macthes) : false;
        
        if (!$validar) {
            
            self::gravar_erro(sprintf('O valor do campo %s deve ter um valor válido', $nome_campo), $mensagem);
        }
        
        return $validar;
    }

    protected static function validar_telefone($valor_para_validar, $valor_regra = null, $nome_campo = '', $mensagem = '')
    {        
        return self::validar_regex($valor_para_validar, (is_null($valor_regra)) ? REGEX_TELEFONE : $valor_regra, $nome_campo, $mensagem);
    }

    protected static function validar_cep($valor_para_validar, $valor_regra = null, $nome_campo = '', $mensagem = '')
    {        
        return self::validar_regex($valor_para_validar, (is_null($valor_regra)) ? REGEX_CEP : $valor_regra, $nome_campo, $mensagem);
    }
    
    protected static function validar_size($valor_para_validar, $valor_regra, $nome_campo = '', $mensagem = '')
    {
        if (!is_array($valor_regra)) 
        {
            $valor_regra = [$valor_regra];
        }
        
        $tamanho_string = strlen($valor_para_validar);
        $validar = true;

        if ($tamanho_string < $valor_regra[0]) 
        {   
            self::gravar_erro(sprintf('O campo %s deve conter no mínimo %s caracteres)', $nome_campo, $valor_regra[0]), $mensagem);
            $validar = false;
        }
        else if ((isset($valor_regra[1])) && ($tamanho_string > $valor_regra[1])) 
        {   
            self::gravar_erro(sprintf('O campo %s deve conter no máximo %s caracteres)', $nome_campo, $valor_regra[1]), $mensagem);
            $validar = false;
        }
        
        return $validar;
    }

    protected static function gravar_erro($mensagem_default, $mensagem_personalizada = '')
    {
        self::$erros[] = (!empty($mensagem_personalizada)) ? $mensagem_personalizada : $mensagem_default;
    }
    
    public static function error()
    {
        return self::$erros;
    }

    public static function __callStatic($name, $arguments)
    {
        self::$erros = [];
        $metodo = 'validar_' . $name;

        if (method_exists(self::class, $metodo)) 
        {   
            if (!is_array($arguments)) 
            {
                $arguments = [$arguments];
            }

            return call_user_func_array(array(self::class, $metodo), $arguments);
        }
    }
}