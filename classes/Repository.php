<?php
class Repository {

    private $tabela;

    public function __construct($tabela)
    {
        $this->tabela = $tabela;    
    }

    private function data()
    {
        return [
            'usuario' => [
                1 => [
                    'userName' => 'Fulano',
                    'cep' => '12345-678',
                    'phoneNumber' => '(11) 98745-1487',
                    'email' => 'email@gmail.com',
                    'senha' => sha1('teste'),
                ],
                2 => [
                    'userName' => 'Ciclano',
                    'cep' => '62345-673',
                    'phoneNumber' => '(11) 98995-2222',
                    'email' => 'testeemail@gmail.com',
                    'senha' => sha1('teste1'),
                ],
            ],
        ];
    }
    
    public function get($field, $value)
    {
        $data = $this->data();

        if (!isset($data[$this->tabela])) 
        {
            return [];
        }

        // buscar pelo campo
        foreach ($data[$this->tabela] as $line) {
            
            // buscar dados na linha
            $search = $this->search($line, $field, $value);

            if ($search === true) {

                return $line;
            }
        }

        return [];
    }

    protected function search($data, $field, $value)
    {
        return ((isset($data[$field])) && ($data[$field] == $value)) ? true : false;
    }
}