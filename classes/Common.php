<?php

function gerarNumeroAleatorio($minimo, $maximo)
{
    list($usec, $sec) = explode(' ', microtime());
    $seed = (float) $sec + ((float) $usec * 100000);

    mt_srand($seed);
    return mt_rand($minimo, $maximo);
}

function filtrarArrayChave($array_original, $array_chaves)
{
    return array_filter($array_original, function($valor, $chave) use ($array_chaves) {

        if (in_array($chave, $array_chaves)) {

            return $valor;
        }
        
    }, ARRAY_FILTER_USE_BOTH);
}