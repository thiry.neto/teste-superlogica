<?php
class Alert {

    private function __construct()
    {   
    }

    public static function warning($mensagem)
    {
        return self::alert($mensagem, 'warning');
    }

    public static function success($mensagem)
    {
        return self::alert($mensagem, 'success');
    }

    protected static function alert($mensagem, $type = 'success')
    {
        $type = 'alert alert-' . $type;
        return '<div class="' . $type . '">' . $mensagem . '</div>';
    }
}