<?php

class Curl {
    
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'PUT';
    
    private $_curl = null;
    private $_url = '';
    private $_headers = [];
    private $_options = [];
    private $_requestType = self::GET;
    private $_fields = [];
    
    public function __construct($url, $headers = array()) 
    {
        
        // setar url
        $this->_url = $url;
        
        // setar opction da url
        $this->setOption([
            CURLOPT_URL => $this->_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        ]);
        
        // set tiver header, então setar
        if (!empty($headers)) 
        {   
            // setar headers
            $this->setHeaders($headers);
        }
    }
    
    public function getUrl() 
    {   
        return $this->_url;
    }
    
    public function setHeaders($headers) 
    {   
        foreach ($headers as $headerKey => $headerValue) 
        {   
            $header = "{$headerKey}: {$headerValue}";
            $this->_headers[] = $header;
        }
    }
    
    public function getHeaders() 
    {   
        return $this->_headers;
    }
    
    public function setOption($options) 
    {   
        foreach ($options as $optionKey => $optionValue) 
        {   
            $this->_options[$optionKey] = $optionValue;
        }
    }
    
    public function getOption() 
    {   
        return $this->_options;
    }
    
    public function get() 
    {   
        // setar o cabeçalho para o tipo de requisição
        $this->setOption([
            CURLOPT_CUSTOMREQUEST => $this->_requestType,
        ]);
        
        return $this->send(self::GET);
    }
    
    public function post($fields = array()) 
    {   
        // identificar os campos que serão enviados
        $this->_fields = $fields;
        
        // setar opções para enviar os campos
        $this->setOption([
            CURLOPT_POSTFIELDS => http_build_query($this->_fields),
        ]);

        // setar o header para aceitar o post
        $this->setHeaders([
            "Content-Type" => "application/x-www-form-urlencoded",
        ]);
        
        return $this->send(self::POST);
    }
    
    private function send($method) 
    {   
        try {
            
            if (!empty($this->_headers)) 
            {
                $this->setOption([
                    CURLOPT_HTTPHEADER => $this->_headers,
                ]);
            }
            
            // iniciar curl
            $this->_curl = curl_init();
            
            // setar as opções de comunicação
            curl_setopt_array($this->_curl, $this->_options);
            
            // executar requisição
            $response = curl_exec($this->_curl);
            
            // erro?
            $err = curl_error($this->_curl);
            
            // fechar comunicação
            curl_close($this->_curl);

            if ($err) {

                throw new Exception($err);
            }
            
            // retornar resposta
            return json_decode($response);
            
        } catch (\Exception $ex) {
            
            return array('error' => $ex->getMessage());
        }
    }
}