<?php

class Data {

    private $tabelas = ['usuario', 'permissoes', 'lancamentos'];

    public function __construct()
    {      
    }

    public function select(string $tabela, array $informacao)
    {
        if (!$this->getTable($tabela)) {

            return false;
        }

        // buscar no respositorio
        $repository = new Repository($tabela);
        $registro = $repository->get('email', $informacao['email']);
        return (!empty($registro)) ? true : false;
    }

    public function insert(string $tabela, array $informacao)
    {
        if (!$this->getTable($tabela)) {

            return false;
        }
        
        return (empty($informacao)) ? false : $this->generateId();
    }

    private function generateId()
    {
        list($usec, $sec) = explode(' ', microtime());
        $seed = (float) $sec + ((float) $usec * 100000);

        mt_srand($seed);
        return mt_rand(1, 10);
    }

    private function getTable($tabela)
    {
        return (in_array($tabela, $this->tabelas)) ? true : false;
    }
}